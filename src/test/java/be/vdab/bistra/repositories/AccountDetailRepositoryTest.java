package be.vdab.bistra.repositories;


import be.vdab.bistra.entities.AccountDetail;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class AccountDetailRepositoryTest {

    @Autowired
    AccountDetailsRepository repository;

    @Test
    public void testGetAll() {
        List<AccountDetail> addresses = repository.findAll();
        assertNotNull(addresses);
        assertEquals(1, addresses.size());
    }


    @Test
    public void testGetById() {
        AccountDetail accountDetail = repository.getOne(1);

        assertNotNull(accountDetail);
        assertEquals("bregt_pompen@hotmail.com", accountDetail.getEmail());
        assertEquals("bregt", accountDetail.getFirstName());
    }

}
