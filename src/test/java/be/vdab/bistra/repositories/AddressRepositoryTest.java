package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Address;
import be.vdab.bistra.entities.Restaurant;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class AddressRepositoryTest {

    @Autowired
    AddressRepository repository;
    @Autowired
    RestaurantRepository restaurantRepository;

    @Test
    public void testGetAll() {
          List<Address> addresses = repository.findAll();
          assertNotNull(addresses);
          assertEquals(2, addresses.size());
    }


    @Test
      public void testGetById() {
          Address address = repository.getOne(1);

          assertNotNull(address);
          assertEquals("boomsestraat", address.getStreet());
          assertEquals("2", address.getBus());
    }

//    @Test
//    public void testFindAddressByRestaurant() {
//        Restaurant restaurant = restaurantRepository.getOne(1);
//        Address address = repository.findAddressByRestaurant(restaurant);
//        Address address2 = new Address(1,"boomsestraat","2",2845,"niel","belgium",restaurant);
//        assertEquals(address,address2);
//    }
}
