package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Dish;
import be.vdab.bistra.entities.Drink;
import be.vdab.bistra.testTools.JPAScriptRunner;
import org.hsqldb.persist.ScriptRunner;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext
@Transactional
public class DrinkRepositoryTest {


    @Autowired
    DrinkRepository repo;


    @Test
    public void testFindById(){
        Drink drink = repo.getOne(1);
        assertEquals("fanta", drink.getName());
    }

    @Test
    public void testGetAll() {
        List<Drink> drinks = repo.findAll();
        assertNotNull(drinks);
        Assertions.assertEquals(1, drinks.size());
    }

}
