package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Order;
import be.vdab.bistra.entities.OrderItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class OrderRepositoryTest {

    @Autowired
    OrderRepository repo;

    @Test
    public void testGetAll() {
        List<Order> orders = repo.findAll();
        assertNotNull(orders);
        Assertions.assertEquals(1, orders.size());
    }
    @Test
    public void testFindById(){
        Order order = repo.getOne(1);
        assertEquals(1,order.getId());
    }
}
