package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Dish;
import be.vdab.bistra.entities.Reservation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class ReservationRepositoryTest {

    @Autowired
    ReservationRepository repo;

    @Test
    public void testFindById(){
        Reservation reservation = repo.getOne(1);
        assertEquals("familie milie", reservation.getName());
    }

    @Test
    public void testGetAll() {
        List<Reservation> reservations = repo.findAll();
        assertNotNull(reservations);
        Assertions.assertEquals(1, reservations.size());
    }

}
