package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Dish;
import be.vdab.bistra.entities.Order;
import be.vdab.bistra.entities.OrderItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class OrderItemRepositoryTest {

    @Autowired
    OrderItemRepository repo;

    @Test
    public void testGetAll() {
        List<OrderItem> orderItems = repo.findAll();
        assertNotNull(orderItems);
        Assertions.assertEquals(1, orderItems.size());
    }


    @Test
    public void testFindById() {
        OrderItem orderItem = repo.getOne(1);
        assertEquals(7.99,orderItem.getPrice(),0.089999771118164);
    }
}
