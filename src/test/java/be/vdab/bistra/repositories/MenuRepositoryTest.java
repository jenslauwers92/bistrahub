package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Menu;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class MenuRepositoryTest {

    @Autowired
    MenuRepository repo;


    @Test
    public void testGetAll() {
        List<Menu> menus = repo.findAll();
        assertNotNull(menus);
        Assertions.assertEquals(1, menus.size());
    }

    @Test
    public void findById() {
        Menu menu = repo.getOne(1);
        assertEquals(1, menu.getId());
    }

}
