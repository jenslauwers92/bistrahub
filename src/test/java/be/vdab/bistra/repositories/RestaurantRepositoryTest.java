package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Dish;
import be.vdab.bistra.entities.Restaurant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class RestaurantRepositoryTest {

    @Autowired
    RestaurantRepository repo;


    @Test
    public void testFindById(){
        Restaurant restaurant = repo.getOne(1);
        assertEquals("Bueno Pizza", restaurant.getName());
    }

    @Test
    public void testGetAll() {
        List<Restaurant> dishes = repo.findAll();
        assertNotNull(dishes);
        Assertions.assertEquals(1, dishes.size());
    }

}
