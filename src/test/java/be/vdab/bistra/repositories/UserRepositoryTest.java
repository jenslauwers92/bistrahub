package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Restaurant;
import be.vdab.bistra.entities.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class UserRepositoryTest {

    @Autowired
    UserRepository repo;

    @Test
    public void testFindById(){
        User user = repo.getOne(1);
        assertEquals("$2a$10$3Nf..JLhSbWx4WJOhYjSHOjLJz6ctkU9qpR1OdbLGoKwj3BVMCUoa",user.getPassword());
    }

    @Test
    public void testGetAll() {
        List<User> users = repo.findAll();
        assertNotNull(users);
        Assertions.assertEquals(1, users.size());
    }


}
