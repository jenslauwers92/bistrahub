package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Dish;
import be.vdab.bistra.entities.TimeSlot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TimeSlotRepositoryTest {

    @Autowired
    TimeSlotRepository repo;


    @Test
    public void testFindById(){
        TimeSlot timeSlot = repo.getOne(1);
        assertEquals("01:05:05", timeSlot.getStartHour().toString());
    }

    @Test
    public void testGetAll() {
        List<TimeSlot> timeSlots = repo.findAll();
        assertNotNull(timeSlots);
        Assertions.assertEquals(1, timeSlots.size());
    }

}
