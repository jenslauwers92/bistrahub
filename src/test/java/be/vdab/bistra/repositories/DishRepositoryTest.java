package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Address;
import be.vdab.bistra.entities.Dish;
import org.hibernate.annotations.SQLInsert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @Author Bregt Pompen
 */

@ExtendWith(SpringExtension.class)
@Transactional
@SpringBootTest
public class DishRepositoryTest {

    @Autowired
    private DishRepository repo;


    @Test
    public void testFindById(){
        Dish dish = repo.findDishById(1);
        assertEquals("Pizza Margaritha", dish.getName());
    }

    @Test
    public void testGetAll() {
        List<Dish> dishes = repo.findAll();
        assertNotNull(dishes);
        Assertions.assertEquals(1, dishes.size());
    }


//    @Test
//    public void testGetDishByName(){
//        List<Beer> beers = repo.getBeerByAlcohol(7);
//        for(Beer b: beers) {
//            Assertions.assertEquals(7,b.getAlcohol(),0.1F);
//        }
//    }

}
