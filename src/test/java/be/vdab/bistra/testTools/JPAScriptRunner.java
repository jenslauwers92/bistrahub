package be.vdab.bistra.testTools;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Script runner/executor for JPA frameworks like Hibernate
 *
 * @author: Ward Truyen
 * @version 1.0.0 , 20-aug-2020
 */
public class JPAScriptRunner {
   EntityManager entityManager;

   public JPAScriptRunner() {
   }

   public JPAScriptRunner(EntityManager entityManager) {
      this.entityManager = entityManager;
   }

   public EntityManager getEntityManager() {
      return entityManager;
   }

   public void setEntityManager(EntityManager entityManager) {
      this.entityManager = entityManager;
   }

   public int executeSqlScript(String filePath){
      if(entityManager == null ) throw new RuntimeException("EntityManager required to run SQL scripts.");

      try {
         //# Load script
         //#============
         //# Open script file
         BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
         //# Read file(lines) & Remove all comments before concatenating to a single String
         AtomicReference<String> scriptAtom = new AtomicReference<>("");
         bufferedReader.lines().map((s) -> s.replaceAll("-{2}.*$", " ").replaceAll("#.*$", " ")).forEach((s) -> scriptAtom.set(scriptAtom.get() + s));
         //# Close File
         bufferedReader.close();

         //# Execute script
         //#===============
         //# for each semi-colon in script, run that part
         String[] script = scriptAtom.get().split(";");
         int rowsUpdated = 0;
         int updatesExecuted = 0;
         for (String scriptlet : script) {
            try {
               Query q = entityManager.createNativeQuery(scriptlet);

               EntityTransaction tx = entityManager.getTransaction();
               tx.begin();
               rowsUpdated = rowsUpdated + q.executeUpdate();
               tx.commit();
               updatesExecuted++;
//            } catch (SQLGrammarException sge) {
//               throw new RuntimeException("Grammar Error while executing script at statement " + (updatesExecuted + 1) + ": " + scriptlet, sge);
            } catch (PersistenceException pe) {
               throw new RuntimeException("Grammar Error while executing script statement " + (updatesExecuted + 1) + ": " + scriptlet, pe);
            } catch (Exception e) {
               throw new RuntimeException("Unknown Error while executing script statement " + (updatesExecuted + 1) + ": " + scriptlet, e);
            }
         }
         //# Print result
         //System.out.println("Executed script \"" + filePath + "\". Rows updated: " + rowsUpdated);
         return rowsUpdated;
      }catch (FileNotFoundException fnfe){
         throw new RuntimeException("File not found: " + filePath, fnfe);
      }catch (IOException ie){
         throw new RuntimeException("Error while reading file: " + filePath, ie);
//      }catch (Exception e){
//         //e.printStackTrace();
//         throw new RuntimeException(e);
      }
   }
}
