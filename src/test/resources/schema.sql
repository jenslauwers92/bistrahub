create table if not exists AccountDetails(
	id          int primary key not null,
	email       varchar(255) not null,
	firstName   varchar(255) not null,
	joinDate    datetime     not null,
	lastName    varchar(255) not null,
	credits     double not null
);

create table if not exists Addresses(
	id          int primary key not null,
	bus         varchar(255) not null,
	city        varchar(255) not null,
	country     varchar(255) not null,
	street      varchar(255) not null,
	zipcode     varchar(255) not null
);

create table if not exists Menus(
	id int primary key not null
);


create table if not exists Adverts(
    id int primary key not null,
    link varchar(128) not null ,
    text varchar(600)
);

create table if not exists Dishes(
	id int primary key not null,
	containsNuts bit not null,
	isVegan bit not null,
	isVegetarian bit not null,
	name varchar(255) not null,
	price double not null,
	type int not null,
	menu_id int null,
-- 	orderItem_id int null,
	constraint FK_Dish_Menu foreign key (menu_id) references Menus (id)
-- 	constraint FK_Dish_OrderItem foreign key (orderItem_id) references OrderItems (id)
);

create table if not exists Drinks(
	id int primary key not null,
	name varchar(255) not null,
	price double not null,
	typeOfDrink int not null,
	menu_id int null,
-- 	orderItem_id int null,
	constraint FK_Drink_Menu foreign key (menu_id) references Menus (id)
-- 	constraint FK_Drink_OrderItem foreign key (orderItem_id) references OrderItems (id)
);


create table if not exists Orders(
                                     id int primary key not null ,
                                     accountDetails_id int null,
--                                      orderItem_id int null,
--                                      constraint FK_Order_OrderItem foreign key (orderItem_id) references OrderItems (id),
                                     constraint FK_Order_AccountDetails foreign key (accountDetails_id) references AccountDetails (id)
);

create table if not exists OrderItems(
                                         id int primary key not null,
                                         amount int not null,
                                         price float not null,
                                         dish_id int null,
                                         drink_id int null,
                                         order_id int not null,

                                         constraint FK_OrderItems_Dishes foreign key (dish_id) references DISHES (id),
                                         constraint FK_OrderItems_Drinks foreign key (drink_id) references DRINKS (id),
                                         constraint FK_OrderItems_Orders foreign key (order_id) references ORDERS (id)
);



create table if not exists TimeSlots(
	id int primary key not null,
	endHour datetime null,
	startHour datetime null
);

create table if not exists Users(
	id int primary key not null,
	isActive bit not null,
	isNotLocked bit not null,
	lastLogin datetime null,
	password varchar(255) not null,
	role int not null,
	username varchar(255) not null,
	accountDetails_id int null,
	constraint UK_Username unique (username),
	constraint FK_Users_AccountDetails foreign key (accountDetails_id) references AccountDetails (id)
);

create table if not exists Restaurants(
	id int primary key not null,
	maxCapacity int not null,
	name varchar(255) not null,
	rating int not null,
	kitchenType int not null,
	address_id int null,
	menu_id int null,
	user_id int null,
	constraint FK_Restaurant_User foreign key (user_id) references Users (id),
	constraint FK_Restaurant_Address foreign key (address_id) references Addresses (id),
	constraint FK_Restaurant_Menu foreign key (menu_id) references Menus (id)
);

create table if not exists Reservations(
	id int primary key not null,
	amountOfPeople int not null,
	date datetime not null,
	name varchar(255) not null,
	accountDetail_id int null,
	restaurant_id int null,
	timeSlot_id int null,
	constraint FK_reservation_timeslots foreign key (timeSlot_id) references TimeSlots (id),
	constraint FK_reservation_restaurants foreign key (restaurant_id) references Restaurants (id),
	constraint FK_Reservation_AccountDetails foreign key (accountDetail_id) references AccountDetails (id)
);

create table if not exists hibernate_sequence(
	next_val bigint null
);
