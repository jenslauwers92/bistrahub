TRUNCATE TABLE RESERVATIONS RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE RESTAURANTS RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE USERS RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE TIMESLOTS RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE ORDERITEMS RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE ORDERS RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE ACCOUNTDETAILS RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE ADDRESSES RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE DISHES RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE DRINKS RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE MENUS RESTART IDENTITY AND COMMIT ;
TRUNCATE TABLE ADS RESTART IDENTITY AND COMMIT ;

INSERT INTO AccountDetails(id,email,firstName,joinDate,lastName,credits)
VALUES (1,'bregt_pompen@hotmail.com','bregt','2020-01-04 05:01:05','pompen',2.1);

INSERT INTO Addresses(id,street,bus,zipcode,city,country)
VALUES (1,'boomsestraat','2','2845','niel','belgium'),
       (2,'javastraat','123','4512','javania','javatopie');

INSERT INTO Menus(id)
VALUES (1);

INSERT INTO Timeslots(id,startHour,endHour)
VALUES (1,'2020-05-04 01:05:05','2020-05-04 05:01:05');

INSERT INTO Users(id,accountDetail_id,password,role,username,lastlogin,isactive,isnotlocked)
VALUES (1,1,'$2a$10$3Nf..JLhSbWx4WJOhYjSHOjLJz6ctkU9qpR1OdbLGoKwj3BVMCUoa',0,'bregtp','2008-12-25 23:30:00',true,true);

INSERT INTO Orders(id,ACCOUNTDETAILS_ID)
VALUES (1,1);


INSERT INTO Drinks(id,name,price,typeOfDrink,MENU_ID)
VALUES (1,'fanta',2.10,0,1);

INSERT INTO Dishes(id,containsNuts,isVegan,isVegetarian,name,price,type,MENU_ID)
VALUES (1,true,false,false,'Pizza Margaritha',7.99,1,1);

INSERT INTO OrderItems(id,amount,price,ORDER_ID,DISH_ID,DRINK_ID)
VALUES (1,1,7.99,1,1,null);

INSERT INTO Restaurants(id,maxcapacity,name,rating,kitchentype,ADDRESS_ID,MENU_ID,USER_ID)
VALUES (1,50,'Bueno Pizza',5,1,1,1,1);

INSERT INTO Reservations(id,amountOfPeople,date,name,ACCOUNTDETAIL_ID,RESTAURANT_ID,TIMESLOT_ID)
VALUES (1,4,'2020-12-25 23:30:00','familie milie',1,1,1);

INSERT INTO Adverts(id,link,text)
VALUES (1,'www.pizzabueno.be','Proef onze overheerlijke pizza''s')
