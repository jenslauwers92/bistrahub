package be.vdab.bistra.entities;

import be.vdab.bistra.entities.enums.KitchenType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Frederik Collignon, Kenny Maes, Bregt Pompen, Pelin Yuzbasioglu
 */
@Entity
@Table(name = "Restaurants")
public class Restaurant implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "kitchenType", nullable = false)
    private KitchenType type;

    @Column(name = "rating", nullable = false)
    private double rating;

    @Column(name = "maxCapacity", nullable = false)
    private int maxCapacity;

    @Column(name = "image")
    private String image;

    @Column(name = "is_parking", nullable = false)
    private boolean isParking;

    @Column(name = "description")
    private String description;

    @OneToOne
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToOne
    @JoinColumn(name = "menu_id")
    private Menu menu;

    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Reservation> reservations = new ArrayList<>();

    @OneToMany(mappedBy = "restaurant")
    private List<OpeningHour> openingHours = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Restaurant() {
    }

    public Restaurant(int id, @NotNull String name, KitchenType type, double rating, @NotNull int maxCapacity, String image, boolean parking, String description, Address address, Menu menu, List<Reservation> reservationList, User user) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.rating = rating;
        this.maxCapacity = maxCapacity;
        this.address = address;
        this.menu = menu;
        this.reservations = reservations;
        this.user = user;
        this.isParking = parking;
        this.image =image;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KitchenType getType() {
        return type;
    }

    public void setType(KitchenType type) {
        this.type = type;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isParking() {
        return isParking;
    }

    public void setParking(boolean parking) {
        isParking = parking;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservationList) {
        this.reservations = reservationList;
    }

    public List<OpeningHour> getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(List<OpeningHour> openingHours) {
        this.openingHours = openingHours;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
