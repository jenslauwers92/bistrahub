package be.vdab.bistra.entities;

import be.vdab.bistra.entities.enums.DishType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

/**
 * @author Frederik Collignon, Kenny Maes, Bregt Pompen, Pelin Yuzbasioglu
 */
@Entity
@Table(name = "Dishes")
public class Dish implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @JsonProperty("dishType")
    @Column(name = "type", nullable = false)
    private DishType type;

    @Column(name = "isVegetarian", nullable = false)
    private boolean isVegetarian;

    @Column(name = "isVegan", nullable = false)
    private boolean isVegan;

    @Column(name = "containsNuts", nullable = false)
    private boolean containsNuts;

    @Positive
    @Column(name = "price", nullable = false)
    private double price;

    @ManyToOne
    @JoinColumn(name = "menu_id")
    private Menu menu;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DishType getType() {
        return type;
    }

    public void setType(DishType type) {
        this.type = type;
    }

    public boolean isVegetarian() {
        return isVegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        isVegetarian = vegetarian;
    }

    public boolean isVegan() {
        return isVegan;
    }

    public void setVegan(boolean vegan) {
        isVegan = vegan;
    }

    public boolean isContainsNuts() {
        return containsNuts;
    }

    public void setContainsNuts(boolean containsNuts) {
        this.containsNuts = containsNuts;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", isVegetarian=" + isVegetarian +
                ", isVegan=" + isVegan +
                ", containsNuts=" + containsNuts +
                ", price=" + price +
                ", menu=" + menu +
                '}';
    }

    //    public OrderItem getOrderItem() {
//        return orderItem;
//    }
//
//    public void setOrderItem(OrderItem orderItem) {
//        this.orderItem = orderItem;
//    }
}
