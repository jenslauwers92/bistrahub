package be.vdab.bistra.entities;

import be.vdab.bistra.entities.enums.Day;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "OpeningsHours")
public class OpeningHour {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Day day;
    private LocalTime startHour;
    private LocalTime endHour;
    private boolean isClosed;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne()
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    // === CONSTRUCTORS ===
    public OpeningHour() {}

    public OpeningHour(Day day, LocalTime startHour, LocalTime endHour, boolean isClosed, Restaurant restaurant) {
        this.day = day;
        this.startHour = startHour;
        this.endHour = endHour;
        this.isClosed = isClosed;
        this.restaurant = restaurant;
    }

    // === GETTERS & SETTERS ===
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public LocalTime getStartHour() {
        return startHour;
    }

    public void setStartHour(LocalTime startHour) {
        this.startHour = startHour;
    }

    public LocalTime getEndHour() {
        return endHour;
    }

    public void setEndHour(LocalTime endHour) {
        this.endHour = endHour;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
