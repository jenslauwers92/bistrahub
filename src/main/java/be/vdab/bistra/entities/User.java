package be.vdab.bistra.entities;


import be.vdab.bistra.entities.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kenny Maes
 * @author Bregt Pompen
 * @version 1.0.2
*/
@Entity
@Table(name ="Users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Integer id;
    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(nullable = false)
    private Role role;

    @Column(nullable = false)
    private boolean isActive;

    @Column(nullable = false)
    private boolean isNotLocked;

    @Transient
    private String[] authorities;
    @PastOrPresent
    private LocalDate lastLogin;

    // === Mappings with other tables ===
    @OneToOne
    private AccountDetail accountDetail;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Restaurant> restaurants = new ArrayList<>();

    // === CONSTRUCTORS ===
    public User() {}

    public User(Integer id, String username, String password, @NotNull @NotBlank Role role, boolean isActive, boolean isNotLocked, String[] authorities, @PastOrPresent LocalDate lastLogin, AccountDetail accountDetail, List<Restaurant> restaurants) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.isActive = isActive;
        this.isNotLocked = isNotLocked;
        this.authorities = authorities;
        this.lastLogin = lastLogin;
        this.accountDetail = accountDetail;
        this.restaurants = restaurants;
    }

    // === GETTERS & SETTERS ===
    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String[] getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String[] authorities) {
        this.authorities = authorities;
    }

    public LocalDate getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDate lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isNotLocked() {
        return isNotLocked;
    }

    public void setNotLocked(boolean notLocked) {
        isNotLocked = notLocked;
    }

    public AccountDetail getAccountDetails() {
        return accountDetail;
    }

    public void setAccountDetails(AccountDetail accountDetail) {
        this.accountDetail = accountDetail;
    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", isActive=" + isActive +
                ", isNotLocked=" + isNotLocked +
                ", authorities=" + Arrays.toString(authorities) +
                ", lastLogin=" + lastLogin +
                ", accountDetails=" + accountDetail +
                ", restaurants=" + restaurants +
                '}';
    }
}
