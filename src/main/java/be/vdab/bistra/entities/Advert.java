package be.vdab.bistra.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * @author Bregt Pompen
 */


@Entity
@Table(name = "Adverts")
public class Advert {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank
    private String link;

    private String text;

    public Advert() {
    }

    public Advert(int id, @NotBlank String link, String text) {
        this.id = id;
        this.link = link;
        this.text = text;
    }

    public int getId() {
        return id;
    }
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

}
