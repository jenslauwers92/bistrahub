package be.vdab.bistra.entities.enums;

/**
 * @Author Frederik Collignon
 */
public enum DrinkType {
SOFT_DRINK, BEER, WINE, COFFEE, TEA, OTHER
}
