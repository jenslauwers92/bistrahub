package be.vdab.bistra.entities.enums;

public enum Day {
    MONDAY("Maandag"),
    TUESDAY("Dinsdag"),
    WEDNESDAY("Woensdag"),
    THURSDAY("Donderdag"),
    FRIDAY("Vrijdag"),
    SATURDAY("Zaterdag"),
    SUNDAY("Zondag");

    private String day;

    Day(String day) {
        this.day = day;
    }
}
