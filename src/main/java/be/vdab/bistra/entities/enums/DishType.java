package be.vdab.bistra.entities.enums;

/**
 * @Author Frederik Collignon
 */
public enum DishType {
    STARTER, MAIN, DESSERT,OTHER
}
