package be.vdab.bistra.entities.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import static be.vdab.bistra.constant.Authority.*;

/**
 * @author Kenny Maes
 */

public enum Role {
    ROLE_USER(USER_AUTHORITIES),
    ROLE_OWNER(OWNER_AUTHORITIES),
    ROLE_SUPER_ADMIN(SUPER_ADMIN_AUTHORITIES);

    private String[] authorities;

    Role(String... authorities) {
        this.authorities = authorities;
    }

    public String[] getAuthorities() {
        return authorities;
    }

    public int toValue() {
        return ordinal();
    }


}
