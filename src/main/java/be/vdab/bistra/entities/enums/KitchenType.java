package be.vdab.bistra.entities.enums;

/**
 * @Author Frederik Collignon
 */
public enum KitchenType {
    MEXICAN, AMERICAN_BURGERS, ITALIAN, INDIAN,OTHER
}
