package be.vdab.bistra.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

/**
 * @author Frederik Collignon, Kenny Maes, Bregt Pompen, Pelin Yuzbasioglu
 */

@Entity
@Table(name = "OrderItems")
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name ="id",nullable = false, updatable = false)
    private int id;
    @Column(nullable = false)
    private int amount;
    @Column(nullable = false)
    private float price;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;
    @OneToOne
    private Dish dish;
    @OneToOne
    private Drink drink;

    public OrderItem() {
    }

    public OrderItem(int id, Order order, Dish dish, Drink drink, int amount, float price) {
        this.id = id;
        this.order = order;
        this.dish = dish;
        this.drink = drink;
        this.amount = amount;
        this.price = price;
    }

    public int getId() {
        return id;
    }
    public Order getOrder() {
        return order;
    }
    public void setOrder(Order order) {
        this.order = order;
    }
    public Dish getDish() {
        return dish;
    }
    public void setDish(Dish dish) {
        this.dish = dish;
    }
    public Drink getDrink() {
        return drink;
    }
    public void setDrink(Drink drink) {
        this.drink = drink;
    }
    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public float getPrice() {
        return price;
    }
    public void setPrice(float price) {
        this.price = price;
    }
}
