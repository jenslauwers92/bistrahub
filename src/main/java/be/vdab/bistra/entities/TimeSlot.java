package be.vdab.bistra.entities;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.List;

/**
 * @author Frederik Collignon, Kenny Maes, Bregt Pompen, Pelin Yuzbasioglu
 */
@Entity
@Table(name = "TimeSlots")
public class TimeSlot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;

    @Column(name = "startHour", nullable = false)
    private LocalTime startHour;

    @Column(name = "endHour", nullable = false)
    private LocalTime endHour;

    public int getId() {
        return id;
    }

    public LocalTime getStartHour() {
        return startHour;
    }

    public void setStartHour(LocalTime startHour) {
        this.startHour = startHour;
    }

    public LocalTime getEndHour() {
        return endHour;
    }

    public void setEndHour(LocalTime endHour) {
        this.endHour = endHour;
    }

}
