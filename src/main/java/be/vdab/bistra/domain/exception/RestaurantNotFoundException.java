package be.vdab.bistra.domain.exception;

/**
 * @author Kenny Maes, Pelin Yuzbasioglu
 */

public class RestaurantNotFoundException extends Exception{
    public RestaurantNotFoundException(String message) {
        super(message);
    }
}
