package be.vdab.bistra.domain.exception;

public class AddressNotFoundException extends Exception {
    public AddressNotFoundException() {
        super();
    }

    public AddressNotFoundException(String message) {
        super(message);
    }
}
