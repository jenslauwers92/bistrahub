package be.vdab.bistra.domain.exception;

/**
 * @author Kenny Maes
 */

public class UsernameExistException extends Exception {

    public UsernameExistException(String message) {
        super(message);
    }
}
