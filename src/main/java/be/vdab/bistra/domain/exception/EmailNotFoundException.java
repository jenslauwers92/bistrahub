package be.vdab.bistra.domain.exception;

/**
 * @author Kenny Maes
 */

public class EmailNotFoundException extends Exception {

    public EmailNotFoundException(String message) {
        super(message);
    }
}
