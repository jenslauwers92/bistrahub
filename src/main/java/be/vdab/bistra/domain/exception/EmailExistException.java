package be.vdab.bistra.domain.exception;

/**
 * @author Kenny Maes
 */

public class EmailExistException extends Exception {

    public EmailExistException(String message) {
        super(message);
    }
}
