package be.vdab.bistra.domain.exception;

/**
 * @author Kenny Maes. Pelin Yuzbasioglu
 */

public class RestaurantNameExistsException extends Exception {
    public RestaurantNameExistsException(String message) {
        super(message);
    }
}
