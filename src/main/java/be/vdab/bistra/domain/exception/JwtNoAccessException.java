package be.vdab.bistra.domain.exception;

public class JwtNoAccessException extends Exception{

    public JwtNoAccessException() {
        super();
    }

    public JwtNoAccessException(String message) {
        super(message);
    }
}
