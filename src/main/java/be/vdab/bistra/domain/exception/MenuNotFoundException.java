package be.vdab.bistra.domain.exception;

public class MenuNotFoundException extends Exception {
    public MenuNotFoundException() {
        super();
    }
    public MenuNotFoundException(String message) {
        super(message);
    }
}
