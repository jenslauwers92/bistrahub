package be.vdab.bistra.domain.exception;

public class DrinkNotFoundException extends Exception {
    public DrinkNotFoundException() {
        super();
    }

    public DrinkNotFoundException(String message) {
        super(message);
    }
}
