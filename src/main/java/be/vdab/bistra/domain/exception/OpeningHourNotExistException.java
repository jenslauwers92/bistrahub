package be.vdab.bistra.domain.exception;

public class OpeningHourNotExistException extends Exception {
    public OpeningHourNotExistException(String message) {
        super(message);
    }
}
