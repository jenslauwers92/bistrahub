package be.vdab.bistra.domain.exception;

/**
 * @author Kenny Maes
 */

public class UserNotFoundException extends Exception {

    public UserNotFoundException(String message) {
        super(message);
    }
}
