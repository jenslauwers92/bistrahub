package be.vdab.bistra.services;

import be.vdab.bistra.domain.exception.AddressNotFoundException;
import be.vdab.bistra.domain.exception.RestaurantNameExistsException;
import be.vdab.bistra.domain.exception.RestaurantNotFoundException;
import be.vdab.bistra.entities.Menu;
import be.vdab.bistra.entities.OpeningHour;
import be.vdab.bistra.entities.Restaurant;
import be.vdab.bistra.entities.User;
import be.vdab.bistra.entities.enums.KitchenType;

import java.util.List;

/**
 * @author Pelin Yuzbasioglu
 */

public interface RestaurantService {
    List<Restaurant> getRestaurants();
    Restaurant findByName(String name);
    Restaurant addNewRestaurant(Restaurant restaurant, String name, KitchenType kitchenType, double rating, int maxCapacity, boolean parking, String description, Menu menu, User user) throws RestaurantNameExistsException, RestaurantNotFoundException;
    List<Restaurant> getRestaurantByUsername(String username);
    Restaurant updateRestaurant(Restaurant restaurant) throws AddressNotFoundException;
    void deleteRestaurant(int id);
    Restaurant getRestaurantByMenuId(Integer id);
    List<OpeningHour>getOpeningHours(Integer restaurantId) throws RestaurantNotFoundException;
    OpeningHour updateOpeningHour(OpeningHour openingHour) throws Exception;

    Restaurant getRestaurant(Integer id) throws RestaurantNotFoundException;

}
