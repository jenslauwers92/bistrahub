package be.vdab.bistra.services;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * @author Kenny Maes
 */

@Service
public class LoginAttemptService {
    public static final int MAXIMUM_NUMBER_OF_ATTEMPTS = 5;
    public static final int ATTEMPT_INCREMENT = 1;
    private LoadingCache<String, Integer> loginAttemptCache;

    public LoginAttemptService() {
        super();
        // set max minutes for the cache and max places (15 minutes and max 100 places in cache
        loginAttemptCache = CacheBuilder.newBuilder().expireAfterWrite(15, MINUTES)
                .maximumSize(100).build(new CacheLoader<String, Integer>() {
                    @Override
                    public Integer load(String key) throws Exception {
                        return 0;
                    }
                });
    }

    // remove the user with attempts from cache
    public void evictUserFromLoginAttemptCache(String username) {
        loginAttemptCache.invalidate(username);
    }

    // add user with attempts to cache
    public void addUserToLoginAttemptCache(String username) {
        int attempts = 0;
        // increment the attempt
        try {
            attempts = ATTEMPT_INCREMENT + loginAttemptCache.get(username);
            System.out.println("Attempts: " + attempts);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        // put user into the cache
        loginAttemptCache.put(username, attempts);
    }

    // return if the user has used his max attempts
    public boolean hasExceededMaxAttempts(String username)  {
        try {
            boolean maxAttemptsReached = loginAttemptCache.get(username) >= MAXIMUM_NUMBER_OF_ATTEMPTS;
            // remove user from cache
            if (maxAttemptsReached) evictUserFromLoginAttemptCache(username);
            return maxAttemptsReached;
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }
}
