package be.vdab.bistra.services;

import be.vdab.bistra.entities.AccountDetail;

import java.util.List;

/**
 * @author Frederik Collignon
 */
public interface AccountDetailsService {
    List<AccountDetail> findAllAccountDetails();
    AccountDetail findById(int id);
    AccountDetail findByEmail(String email);
    AccountDetail findByUserName(String username);


    AccountDetail updateCredits(String username, Double addition);
    //AccountDetails updateCredits(double addition, String email);
}
