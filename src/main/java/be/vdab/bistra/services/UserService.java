package be.vdab.bistra.services;

import be.vdab.bistra.domain.exception.EmailExistException;
import be.vdab.bistra.domain.exception.EmailNotFoundException;
import be.vdab.bistra.domain.exception.UserNotFoundException;
import be.vdab.bistra.domain.exception.UsernameExistException;
import be.vdab.bistra.entities.User;
import be.vdab.bistra.entities.enums.Role;

import javax.mail.MessagingException;
import java.util.List;

public interface UserService {

    User register(User user) throws UserNotFoundException, UsernameExistException, EmailExistException, MessagingException;

    List<User> getUsers();

    User findByUsername(String username);

    User findByEmail(String email);

    User addNewUser(User user, Role role, boolean isNotLocked, boolean isActive) throws UserNotFoundException, UsernameExistException, EmailExistException;

    User updateUser(String currentUsername, String firstName, String lastName, String userName, String email, double credits,  Role role, boolean isNotLocked, boolean isActive) throws UserNotFoundException, UsernameExistException, EmailExistException;

    void deleteUser(int id);

    void resetPassword(String email) throws MessagingException, EmailNotFoundException;
}
