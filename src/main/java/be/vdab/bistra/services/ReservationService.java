package be.vdab.bistra.services;


import be.vdab.bistra.entities.*;
import be.vdab.bistra.models.ReservationDto;

import java.util.List;

/**
 * @author Pelin Yuzbasioglu
 */

public interface ReservationService {
    Reservation getReservationById(Integer id);
    Reservation addReservation(Reservation reservation);
    List<ReservationDto> getReservationsByUser(User user);
    List<ReservationDto> getAllByRestaurants(Restaurant restaurant);
}
