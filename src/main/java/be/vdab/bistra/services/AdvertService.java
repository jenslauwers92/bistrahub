package be.vdab.bistra.services;

import be.vdab.bistra.entities.Advert;
import be.vdab.bistra.repositories.AdvertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Bregt Pompen
 */

@Service
public class AdvertService {

    private final AdvertRepository advertRepository;


    @Autowired
    public AdvertService(AdvertRepository advertRepository) {
        this.advertRepository = advertRepository;
    }

    public List<Advert> getLast5Adverts(){

        List<Advert> adverts = advertRepository.findAll();
        if(adverts.size() >= 4) {
            Collections.reverse(adverts);
            return  adverts.stream().limit(4).collect(Collectors.toList());
        }
        return adverts;
    }


    public Advert add(Advert advert)  {

        List<Advert> adverts = advertRepository.findAll();

        if(adverts.size() >= 20){
            advertRepository.delete(adverts.stream().findFirst().get());
        }
        // requestbody set parameters automatisch
        if(!advert.getLink().startsWith("https://")){
            StringBuilder sb = new StringBuilder("https://");
            if(!advert.getLink().contains("www.")){
                StringBuilder sb2 = new StringBuilder("https://www.");
                advert.setLink(sb2+advert.getLink());
            } else advert.setLink(sb + advert.getLink());
        }
        return advertRepository.save(advert);
    }

}
