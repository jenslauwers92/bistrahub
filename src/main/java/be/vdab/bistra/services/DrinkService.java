package be.vdab.bistra.services;

import be.vdab.bistra.domain.exception.DrinkNotFoundException;
import be.vdab.bistra.entities.Drink;

import java.util.List;

/**
 * @author Pelin Yuzbasioglu
 */

public interface DrinkService {
    Drink updateDrink(Drink drink);
    Drink addDrink(Drink drink);
    void deleteDrink(int id);
    List<Drink> getAllDrinks();
    List<Drink> getDrinksByMenuId(Integer menu_id);
    List<Drink> getDrinksByType(Integer menu_id);
    Drink getDrinkById(int id) throws DrinkNotFoundException;
}
