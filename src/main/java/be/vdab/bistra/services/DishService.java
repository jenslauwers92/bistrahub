package be.vdab.bistra.services;

import be.vdab.bistra.entities.Dish;

import java.util.List;

/**
 * @author Pelin Yuzbasioglu
 */

public interface DishService {
    Dish addDish(Dish dish);
    Dish updateDish(Dish dish);
    void deleteDish(int id);
    List<Dish> getAllDishes();
    List<Dish> getDishesByMenuId(Integer menu_id);
    Dish getDishById(int id);
    List<Dish> getDishesByType(Integer menu_id);
}
