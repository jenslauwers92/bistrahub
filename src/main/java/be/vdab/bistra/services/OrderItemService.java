package be.vdab.bistra.services;

import be.vdab.bistra.entities.OrderItem;

import java.security.Principal;
import java.util.List;
/*
** @author Bregt Pompen
 */
public interface OrderItemService {

    List<OrderItem> ProcessAll(List<OrderItem> orderItems, String username);

}
