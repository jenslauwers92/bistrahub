package be.vdab.bistra.services.impl;

import be.vdab.bistra.entities.Order;
import be.vdab.bistra.entities.OrderItem;
import be.vdab.bistra.entities.User;
import be.vdab.bistra.repositories.OrderItemRepository;
import be.vdab.bistra.repositories.OrderRepository;
import be.vdab.bistra.repositories.UserRepository;
import be.vdab.bistra.services.OrderItemService;
import be.vdab.bistra.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

/**
 ** @author Bregt Pompen
 */

@Service
@Transactional
public class OrderItemServiceImpl implements OrderItemService {

    OrderItemRepository orderItemRepository;

    OrderRepository orderRepository;

    UserService userService;

    UserRepository userRepository;





    @Autowired
    public OrderItemServiceImpl(OrderItemRepository orderItemRepository,OrderRepository orderRepository
            ,UserService userService,UserRepository userRepository) {
        this.orderItemRepository = orderItemRepository;
        this.orderRepository = orderRepository;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @Override
    public List<OrderItem> ProcessAll(List<OrderItem> orderItems, String username) {
        Order order = new Order();
        User user = userService.findByUsername(username);

        double totalPrice = orderItems.stream().mapToDouble(orderItem -> (double) orderItem.getPrice()).sum();
        System.out.println(totalPrice);
        double newPrice = user.getAccountDetails().getCredits() - (totalPrice/10);

        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        String format = df.format(newPrice);
        format = format.replace(",", ".");
        newPrice = Double.parseDouble(format);

        user.getAccountDetails().setCredits(newPrice);
        User savedUser = userRepository.save(user);

        order.setUser(savedUser);
        Order savedOrder = orderRepository.save(order);

        orderItems.forEach(orderItem -> orderItem.setOrder(savedOrder));
        orderItemRepository.saveAll(orderItems);

        return orderItems;
    }
}
