package be.vdab.bistra.services.impl;

import be.vdab.bistra.domain.exception.DrinkNotFoundException;
import be.vdab.bistra.entities.Drink;
import be.vdab.bistra.repositories.*;
import be.vdab.bistra.services.DrinkService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * @author Pelin Yuzbasioglu
 */

@Service
@Transactional
public class DrinkServiceImpl implements DrinkService {
    DrinkRepository drinkRepository;
    MenuRepository menuRepository;

    public DrinkServiceImpl(DrinkRepository drinkRepository, MenuRepository menuRepository) {
        this.drinkRepository = drinkRepository;
        this.menuRepository = menuRepository;
    }

    @Override
    public List<Drink> getAllDrinks() {
        return drinkRepository.findAll();
    }

    @Override
    public Drink getDrinkById(int id) throws DrinkNotFoundException {
        Drink drink = drinkRepository.getOne(id);
        if (drink == null) {
            throw new DrinkNotFoundException();
        }
        return drink;
    }

    @Override
    public Drink updateDrink(Drink drink) {
        if (drinkRepository.findById(drink.getId()).isPresent()) {
            System.out.println(drink);
            return drinkRepository.save(drink);
        } else {
            throw new IllegalArgumentException("Drink service impl could not save the restaurant");
        }
    }

    @Override
    public List<Drink> getDrinksByMenuId(Integer menu_id) {

        return drinkRepository.findDrinksByMenu_Id(menu_id);
    }

    @Override
    public List<Drink> getDrinksByType(Integer menu_id) {
        return drinkRepository.findDrinksByMenu_IdOrderByType(menu_id);
    }

    @Override
    public Drink addDrink(Drink drink) {
        return drinkRepository.save(drink);
    }

    @Override
    public void deleteDrink(int id) {
        drinkRepository.deleteById(id);
    }

}
