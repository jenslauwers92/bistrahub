package be.vdab.bistra.services.impl;

import be.vdab.bistra.entities.Dish;
import be.vdab.bistra.entities.enums.DishType;
import be.vdab.bistra.repositories.*;
import be.vdab.bistra.services.DishService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Pelin Yuzbasioglu
 */

@Service
@Transactional
public class DishServiceImpl implements DishService {
    DishRepository dishRepository;
    MenuRepository menuRepository;

    public DishServiceImpl(DishRepository dishRepository, MenuRepository menuRepository) {
        this.dishRepository = dishRepository;
        this.menuRepository = menuRepository;
    }

    @Override
    public Dish addDish(Dish dish) {
        return dishRepository.save(dish);
    }

    @Override
    public Dish updateDish(Dish dish) {
        if (dishRepository.findById(dish.getId()).isPresent()) {
            System.out.println(dish);
            return dishRepository.save(dish);
        } else {
            throw new IllegalArgumentException("Dish service impl could not save the restaurant");
        }
    }

    @Override
    public void deleteDish(int id) {
        dishRepository.deleteById(id);
    }

    @Override
    public List<Dish> getAllDishes() {
        return dishRepository.findAll();
    }

    @Override
    public List<Dish> getDishesByMenuId(Integer menu_id) {
        return dishRepository.findDishesByMenu_Id(menu_id);
    }

    @Override
    public Dish getDishById(int id) {
        return dishRepository.findDishById(id);
    }

    @Override
    public List<Dish> getDishesByType(Integer menu_id) {
        return dishRepository.findDishesByMenu_IdOrderByType(menu_id);
    }
}
