package be.vdab.bistra.services.impl;

import be.vdab.bistra.models.ReservationDto;
import be.vdab.bistra.repositories.ReservationRepository;
import be.vdab.bistra.services.ReservationService;
import be.vdab.bistra.entities.Reservation;
import be.vdab.bistra.entities.Restaurant;
import be.vdab.bistra.entities.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Pelin Yuzbasioglu
 */

@Service
public class ReservationServiceImpl implements ReservationService {
    private final ReservationRepository reservationRepository;


    public ReservationServiceImpl(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Override
    public Reservation getReservationById(Integer id) {
        return reservationRepository.findReservationById(id);
    }

    @Override
    public Reservation addReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }

    @Override
    public List<ReservationDto> getReservationsByUser(User user) {
        List<Reservation> allByUser = reservationRepository.findReservationsByUser(user);
        return allByUser.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public List<ReservationDto> getAllByRestaurants(Restaurant restaurant) {
        List<Reservation> allByRestaurant = reservationRepository.findReservationsByRestaurant(restaurant);
        return allByRestaurant.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    private ReservationDto convertToDto(Reservation reservation) {
        return new ReservationDto(reservation.getId(), reservation.getName(),reservation.getAmountOfPeople(),reservation.getDate(),reservation.getRestaurant().getId(), reservation.getUser().getId());
    }
}
