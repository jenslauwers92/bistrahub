package be.vdab.bistra.services.impl;

import be.vdab.bistra.entities.*;
import be.vdab.bistra.repositories.MenuRepository;
import be.vdab.bistra.services.MenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Pelin Yuzbasioglu
 */

@Service
@Transactional
public class MenuServiceImpl implements MenuService {
    public MenuRepository menuRepository;

    public MenuServiceImpl(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    @Override
    public Menu findMenuById(int id) {
        return menuRepository.getMenuById(id);
    }

    @Override
    public Menu updateMenu(Restaurant restaurant) {
        return null;
    }

    @Override
    public Menu getMenuByRestaurantName() {
        return null;
    }


}
