package be.vdab.bistra.services.impl;



import be.vdab.bistra.domain.exception.AddressNotFoundException;
import be.vdab.bistra.domain.exception.OpeningHourNotExistException;
import be.vdab.bistra.domain.exception.RestaurantNameExistsException;
import be.vdab.bistra.domain.exception.RestaurantNotFoundException;
import be.vdab.bistra.entities.*;
import be.vdab.bistra.entities.enums.Day;
import be.vdab.bistra.entities.enums.KitchenType;
import be.vdab.bistra.repositories.*;
import be.vdab.bistra.services.RestaurantService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static be.vdab.bistra.constant.RestaurantConstant.NO_RESTAURANT_FOUND;
import static be.vdab.bistra.constant.RestaurantConstant.RESTAURANT_NAME_ALREADY_EXISTS;

/**
 * @author Kenny Maes, Pelin Yuzbasioglu
 */

@Service
@Transactional
public class RestaurantServiceImpl implements RestaurantService {
    private final RestaurantRepository restaurantRepository;
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final MenuRepository menuRepository;
    private final OpeningHourRepository openingHourRepository;

    public RestaurantServiceImpl(RestaurantRepository restaurantRepository, UserRepository userRepository, AddressRepository addressRepository, MenuRepository menuRepository, OpeningHourRepository openingHourRepository) {
        this.restaurantRepository = restaurantRepository;
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
        this.menuRepository = menuRepository;
        this.openingHourRepository = openingHourRepository;
    }

    @Override
    public List<Restaurant> getRestaurants() {
        return restaurantRepository.findAll();
    }

    @Override
    public Restaurant findByName(String name) {
        return restaurantRepository.getRestaurantByName(name);
    }

    @Override
    public Restaurant addNewRestaurant(Restaurant restaurant, String name, KitchenType kitchenType, double rating, int maxCapacity, boolean parking, String description, Menu menu, User user) throws RestaurantNotFoundException, RestaurantNameExistsException {
        Restaurant newRestaurant = createRestaurant(StringUtils.EMPTY, restaurant, kitchenType, rating, maxCapacity, parking, description);
        addressRepository.save(newRestaurant.getAddress());
        Menu newMenu = menuRepository.save(menu);
        newRestaurant.setUser(user);
        newRestaurant.setMenu(newMenu);
        Restaurant savedRestaurant = restaurantRepository.save(newRestaurant);
        List<OpeningHour> openingHours = new ArrayList<>();
        // set 7 days Monday - Sunday
        for (Day day : Day.values()) {
            openingHours.add(new OpeningHour(day, LocalTime.of(0, 0), LocalTime.of(0, 0), true, savedRestaurant));
        }
        openingHourRepository.saveAll(openingHours);
        return savedRestaurant;
    }


    @Override
    public List<Restaurant> getRestaurantByUsername(String username) {
        User user = userRepository.findUserByUsername(username);
        return restaurantRepository.getRestaurantsByUser(user);
    }

    @Override
    public Restaurant updateRestaurant(Restaurant restaurant) throws AddressNotFoundException {
        if (restaurantRepository.findById(restaurant.getId()).isPresent()) {
            if (addressRepository.findById(restaurant.getAddress().getId()).isPresent()) {
                addressRepository.save(restaurant.getAddress());
                return restaurantRepository.save(restaurant);
            }
            throw new AddressNotFoundException();
        } else {
            throw new IllegalArgumentException("Restaurant service impl could not save the restaurant");
        }
    }

    @Override
    public void deleteRestaurant(int id) {
        restaurantRepository.deleteById(id);
    }

    @Override
    public Restaurant getRestaurantByMenuId(Integer id) {
        return restaurantRepository.getRestaurantByMenu_Id(id);
    }


    public List<OpeningHour> getOpeningHours(Integer restaurantId) throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantRepository.getRestaurantById(restaurantId);
        if (restaurant == null) {
            throw new RestaurantNotFoundException(NO_RESTAURANT_FOUND);
        }
        return restaurant.getOpeningHours();
    }

    @Override
    public OpeningHour updateOpeningHour(OpeningHour openingHour) throws Exception {
        if (openingHour.getId() == 0) throw new OpeningHourNotExistException("Openingsuur kan niet aangepast worden");
        Optional<OpeningHour> currentOpeningHour = openingHourRepository.findById(openingHour.getId());
        currentOpeningHour.ifPresent(data -> {
            openingHour.setRestaurant(data.getRestaurant());
        });

        if (currentOpeningHour.isPresent()) {
            return openingHourRepository.save(openingHour);

        } else {
            throw new IllegalArgumentException("Openingsuur kan niet opgeslagen worden");
        }
    }

    @Override
    public Restaurant getRestaurant(Integer id) throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantRepository.getRestaurantById(id);
        if (restaurant == null) throw new RestaurantNotFoundException("Geen restaurant gevonden");
        return restaurant;
    }

    private Restaurant createRestaurant (String restaurantName, Restaurant restaurant, KitchenType kitchenType, double rating, int maxCapacity, boolean parking, String description) throws RestaurantNotFoundException, RestaurantNameExistsException {



        Restaurant currentRestaurant = validateNewRestaurant(restaurantName, restaurant.getName());
        Address address = new Address();

        if (currentRestaurant == null) {
            currentRestaurant = new Restaurant();
        }

        address.setStreet(restaurant.getAddress().getStreet());
        address.setBus(restaurant.getAddress().getBus());
        address.setZipcode(restaurant.getAddress().getZipcode());
        address.setCity(restaurant.getAddress().getCity());
        address.setCountry(restaurant.getAddress().getCountry());

        currentRestaurant.setName(restaurant.getName().toLowerCase());
        currentRestaurant.setType(kitchenType);
        currentRestaurant.setMaxCapacity(maxCapacity);
        currentRestaurant.setRating(rating);
        currentRestaurant.setParking(parking);
        currentRestaurant.setAddress(address);
        currentRestaurant.setDescription(description);
        currentRestaurant.setImage(restaurant.getImage());
        return currentRestaurant;
    }


    private Restaurant validateNewRestaurant(String currentRestaurantName, String newName)
            throws RestaurantNotFoundException, RestaurantNameExistsException {

        Restaurant restaurantByNewName = findByName(newName);

        if (StringUtils.isNotBlank(currentRestaurantName)) {
            Restaurant currentRestaurant = findByName(currentRestaurantName);
            if (currentRestaurant == null) {
                throw new RestaurantNotFoundException(NO_RESTAURANT_FOUND + currentRestaurantName);
            }

            if (restaurantByNewName != null) {
                throw new RestaurantNameExistsException(RESTAURANT_NAME_ALREADY_EXISTS);
            }
            return currentRestaurant;
        } else {
            if (restaurantByNewName != null) {
                throw new RestaurantNameExistsException(RESTAURANT_NAME_ALREADY_EXISTS);
            }
            return null;
        }
    }

}

