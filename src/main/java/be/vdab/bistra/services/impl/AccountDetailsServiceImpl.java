package be.vdab.bistra.services.impl;

import be.vdab.bistra.entities.AccountDetail;
import be.vdab.bistra.repositories.AccountDetailsRepository;
import be.vdab.bistra.repositories.UserRepository;
import be.vdab.bistra.services.AccountDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

/**
 * @author Frederik Collignon
 */
@Service
@Transactional
public class AccountDetailsServiceImpl implements AccountDetailsService {
    private AccountDetailsRepository accountDetailsRepository;
    private UserRepository userRepository;

    @Autowired
    public AccountDetailsServiceImpl(AccountDetailsRepository accountDetailsRepository, UserRepository userRepository) {
        this.accountDetailsRepository = accountDetailsRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<AccountDetail> findAllAccountDetails() {
        return accountDetailsRepository.findAll();
    }

    @Override
    public AccountDetail findById(int id) {
        return accountDetailsRepository.findAccountDetailById(id);
    }

    @Override
    public AccountDetail findByEmail(String email) {
        return accountDetailsRepository.findAccountDetailByEmail(email);
    }

    @Override
    public AccountDetail findByUserName(String username) { return userRepository.findUserByUsername(username).getAccountDetails(); }

    @Override
    public AccountDetail updateCredits(String username, Double addition) {
        AccountDetail currentAccountDetail = userRepository.findUserByUsername(username).getAccountDetails();

        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.FLOOR);
        addition = Double.parseDouble(df.format(addition));

        currentAccountDetail.setCredits(currentAccountDetail.getCredits() + addition);
        accountDetailsRepository.save(currentAccountDetail);
        return currentAccountDetail;
    }

}
