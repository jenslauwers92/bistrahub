package be.vdab.bistra.services.impl;

import be.vdab.bistra.domain.UserPrincipal;
import be.vdab.bistra.domain.exception.*;
import be.vdab.bistra.entities.*;
import be.vdab.bistra.entities.User;
import be.vdab.bistra.entities.enums.Role;
import be.vdab.bistra.repositories.*;
import be.vdab.bistra.services.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

import static be.vdab.bistra.constant.UserConstant.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * @author Kenny Maes. Bregt Pompen
 */

@Service
@Transactional
@Qualifier("UserDetailsService")
public class UserServiceImpl implements UserService, UserDetailsService {

    private Logger LOGGER = LoggerFactory.getLogger(getClass());
    private UserRepository userRepository;
    private AccountDetailsRepository accountDetailsRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private LoginAttemptService loginAttemptService;
    private EmailService emailService;

    // === CONSTRUCTORS ===
    @Autowired
    public UserServiceImpl(UserRepository userRepository, AccountDetailsRepository accountDetailsRepository, BCryptPasswordEncoder passwordEncoder,
                           LoginAttemptService loginAttemptService, EmailService emailService) {
        this.userRepository = userRepository;
        this.accountDetailsRepository = accountDetailsRepository;
        this.passwordEncoder = passwordEncoder;
        this.loginAttemptService = loginAttemptService;
        this.emailService = emailService;
    }

    // === OVERRIDE METHODS ===
    /**
     * When the user try to login this method will be called by spring to check the security level of that user
     * @param username the username to check for in database
     * @return UserPrincipal of a user
     * @throws UsernameNotFoundException if the username has no match in database this will be thrown
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findUserByUsername(username);
        if (user == null) {
            LOGGER.error(NO_USER_FOUND_BY_USERNAME + username);
            throw new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME + username);
        } else {
            user.setAuthorities(user.getRole().getAuthorities());
            // check total login attempts
            validateLoginAttempt(user);
            // add lastLoginDate to the user and then save to database
            user.setLastLogin(LocalDate.now());
            userRepository.save(user);
            UserPrincipal userPrincipal = new UserPrincipal(user);
            LOGGER.info("Returning found user by username: " + username);
            return userPrincipal;
        }
    }

    /**
     * Registration of a new user
     * @param user the information of the new user
     * @return the user that is saved to the database with id
     * @throws UserNotFoundException will never throw the currentUser is always empty
     * @throws UsernameExistException throws if a user already exist with that username
     * @throws EmailExistException throws if a user already exist with that email
     * @throws MessagingException throws if the is something wrong with sending the email with the password
     */
    @Override
    public User register(User user) throws UserNotFoundException, UsernameExistException, EmailExistException, MessagingException {
        String password = generatePassword();
        LOGGER.info("generated password: " + password);
        Role role = Role.ROLE_USER;
        if (user.getRole() != null) role = user.getRole();
        User newUser = createOrGetUser(EMPTY, user, password, role, true, true);
        System.out.println(newUser.getAccountDetails());
        emailService.sendNewPasswordEmail(newUser.getAccountDetails().getFirstName(), password, newUser.getAccountDetails().getEmail());

        //TODO: need to be set at the same point where the user is saved ? if something is wrong accountDetails and user gets rollback....
        // save the accountDetails before the user is saved
        System.out.println("new User: " + newUser);
        accountDetailsRepository.save(newUser.getAccountDetails());
        return userRepository.save(newUser);
    }

    /**
     * adding a new user when already logged in
     * @param user the user that need to be created
     * @param role the role of that new user
     * @param isNotLocked if the account is locked
     * @param isActive if the account is active
     * @return a new user
     * @throws UserNotFoundException if a user already exists
     * @throws UsernameExistException if a username already exists
     * @throws EmailExistException iif a email already exists
     */
    @Override
    public User addNewUser(User user, Role role, boolean isNotLocked, boolean isActive) throws UserNotFoundException, UsernameExistException, EmailExistException {
        String password = generatePassword();
        LOGGER.info("generated password: " + password);
        User newUser = createOrGetUser(EMPTY, user, password, role, isActive, isNotLocked);
        // save accountDetails before the user is saved
        accountDetailsRepository.save(newUser.getAccountDetails());
        return userRepository.save(newUser);
    }

    /**
     * Update a user
     * @param currentUsername the user that needs to be updated - if not exists it wil throw UserNotFoundException
     * @param firstName the first name of that user
     * @param lastName the last name of that user
     * @param userName the username of that user
     * @param email the email of that user
     * @param role the role of a user - same or updated
     * @param isNotLocked same or change if that user is locked or not
     * @param isActive same or change if tha user is active or not
     * @return a updated user
     * @throws UserNotFoundException throws if the currentUsername not matched
     * @throws UsernameExistException throws if a username already exists when try to change in an update
     * @throws EmailExistException throws if a email already exists when try to change in an update
     */
    @Override
    public User updateUser(String currentUsername, String firstName, String lastName, String userName, String email, double credits, Role role, boolean isNotLocked, boolean isActive) throws UserNotFoundException, UsernameExistException, EmailExistException {
        User user = new User();
        AccountDetail accountDetail = new AccountDetail();
        accountDetail.setFirstName(firstName);
        accountDetail.setLastName(lastName);
        accountDetail.setEmail(email);
        accountDetail.setCredits(credits);
        user.setUsername(userName);
        user.setAccountDetails(accountDetail);

        User currentUser = createOrGetUser(currentUsername, user, null, role, isActive, isNotLocked);
        return userRepository.save(currentUser);
    }

    /**
     * Get all users in a list
     * @return returns a list with users if exist or empty list if no users are in database
     */
    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    /**
     * find user by username
     * @param username the username that need to be searched for
     * @return a user if username exists or null if not
     */
    @Override
    public User findByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    /**
     * find a user by email
     * @param email the email from a user need to search for
     * @return a user if email exist or null if not
     */
    @Override
    public User findByEmail(String email) {
        return userRepository.findUserByAccountDetail_Email(email);
    }

    /**
     * delete a user from the database with id
     * @param id the id of a user that need to be deleted
     */
    @Override
    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }

    /**
     * Reset the password for a user with that email
     * @param email the email where the password need to be reset
     * @throws MessagingException throws when something is wrong with sending the email
     * @throws EmailNotFoundException throws when the email not exists
     */
    @Override
    public void resetPassword(String email) throws MessagingException, EmailNotFoundException {
        User user = userRepository.findUserByAccountDetail_Email(email);
        if (user == null) {
            throw new EmailNotFoundException(NO_USER_FOUND_BY_EMAIL + email);
        }
        String password = generatePassword();
        user.setPassword(encodePassword(password));
        userRepository.save(user);
        emailService.sendNewPasswordEmail(user.getAccountDetails().getFirstName(), password, user.getAccountDetails().getEmail());
    }

    /// === PRIVATE METHODS ===
    /**
     * Get the role true a string
     * @param role the name of a role
     * @return a role object
     */
    private Role getRoleEnumName(String role) {
        return Role.valueOf(role.toUpperCase());
    }

    /**
     * Validate if a user has reached his max login attempts
     * @param user the user that needs to be validated
     */
    private void validateLoginAttempt(User user) {
        // check if user is not locked
        if (user.isNotLocked()) {
            // check if the max attempts are reached and set userNotLocked to false if they are
            user.setNotLocked(!loginAttemptService.hasExceededMaxAttempts(user.getUsername().toLowerCase()));
        } else {
            // remove the cache for this username
            //TODO: Check if this is necessary ?
            loginAttemptService.evictUserFromLoginAttemptCache(user.getUsername());
        }
    }

    /**
     * Encode the password from a user to save it in database
     * @param password the normal password from a user before encoding
     * @return encode password in a string
     */
    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    /**
     * generates a random password to send in mail when user is created or when user ask for new password with resetPassword method
     * @return a random string of 10 chars numbers and alphabetic
     */
    private String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(10);
    }

    private String generateUserId() {
        return RandomStringUtils.randomNumeric(10);
    }

    /**
     * Validation if a user already exists with a particular username or email
     * @param currentUsername the username where a user is logged in with - is empty when new account is created
     * @param newUsername the username of a new user or if username needs to be updated
     * @param newEmail the email of a new user or if email needs to be updated
     * @return a new user or a existing user currentUsername is a match en not empty/blank
     * @throws UserNotFoundException throws f user is not found with currentUsername
     * @throws UsernameExistException throws if newUsername already exists in the database
     * @throws EmailExistException throws if a email already exists in the database
     */
    private User validateNewUsernameAndEmail(String currentUsername, String newUsername, String newEmail) throws UserNotFoundException, UsernameExistException, EmailExistException {
        User userByNewUsername = findByUsername(newUsername);
        User userByNewEmail = findByEmail(newEmail);

        if (StringUtils.isNotBlank(currentUsername)) {
            User currentUser = findByUsername(currentUsername);
            if (currentUser == null) {
                throw new UserNotFoundException(NO_USER_FOUND_BY_USERNAME + currentUsername);
            }

            // check if new username is not null and if the currentUser is not the same as the new username
            if (userByNewUsername != null && !currentUser.getId().equals(userByNewUsername.getId())) {
                throw new UsernameExistException(USERNAME_ALREADY_EXISTS);
            }
            // check if new userByEmail is not null and if the currentUser is not the same as the new userByEmail
            if (userByNewEmail != null && !currentUser.getId().equals(userByNewEmail.getId())) {
                throw new EmailExistException(EMAIL_ALREADY_EXISTS);
            }
            return currentUser;
        } else {
            if (userByNewUsername != null) {
                throw new UsernameExistException(USERNAME_ALREADY_EXISTS);
            }
            if (userByNewEmail != null) {
                throw new EmailExistException(EMAIL_ALREADY_EXISTS);
            }
            return null;
        }
    }

    /**
     * To create a new user or give back a existing user
     * @param currentUsername username that a user has if he is logged in
     * @param user the object with data from a user
     * @param password the password that was created for a user or that a user want to change to
     * @param role role of a user
     * @param isActive if a user is active or not
     * @param isNotLocked if a user is blocked or not where true will say user is not blocked
     * @return returns a user, existing or new
     * @throws UserNotFoundException throws when the currentUsername is not a match with database
     * @throws UsernameExistException throws when a user already exist with a username in database
     * @throws EmailExistException throws when a user already exist with a email in database
     */
    private User createOrGetUser(String currentUsername, User user, String password, Role role, boolean isActive, boolean isNotLocked) throws UserNotFoundException, UsernameExistException, EmailExistException {
        User currentUser = validateNewUsernameAndEmail(currentUsername, user.getUsername(), user.getAccountDetails().getEmail());
        AccountDetail accountDetail = new AccountDetail();
        if (currentUser == null) {
            // setup for new user only
            String encodedPassword = encodePassword(password);
            currentUser = new User();
            currentUser.setPassword(encodedPassword);
        }
        // setup account details, new or override
        accountDetail.setFirstName(user.getAccountDetails().getFirstName());
        accountDetail.setLastName(user.getAccountDetails().getLastName());
        accountDetail.setEmail(user.getAccountDetails().getEmail());
        accountDetail.setCredits(user.getAccountDetails().getCredits());
        accountDetail.setJoinDate(LocalDate.now());
        System.out.println(accountDetail);
        // Setup user for both new or override
        currentUser.setUsername(user.getUsername().toLowerCase());
        currentUser.setActive(isActive);
        currentUser.setNotLocked(isNotLocked);
        currentUser.setRole(role);
        currentUser.setAuthorities(role.getAuthorities());
        currentUser.setAccountDetails(accountDetail);
        System.out.println(currentUser);

        return currentUser;
    }
}
