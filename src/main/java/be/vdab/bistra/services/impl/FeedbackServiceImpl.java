package be.vdab.bistra.services.impl;

import be.vdab.bistra.models.FeedbackList;
import be.vdab.bistra.entities.Feedback;
import be.vdab.bistra.entities.Restaurant;
import be.vdab.bistra.repositories.FeedbackRepository;
import be.vdab.bistra.services.FeedbackService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Pelin Yuzbasioglu
 */

@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    public FeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public FeedbackList getFeedback(Restaurant restaurant) {
        List<Feedback> allByRestaurant = feedbackRepository.findAllByRestaurant(restaurant);
        Double average = allByRestaurant.stream().map(Feedback::getRating).collect(Collectors.averagingDouble(Double::valueOf));
        return new FeedbackList(allByRestaurant.size(), average, allByRestaurant);
    }

    @Override
    public Integer countAllByRestaurant(Restaurant restaurant) {
        return feedbackRepository.countAllByRestaurant(restaurant);
    }

    @Override
    public List<Feedback> getAllByRestaurant(Restaurant restaurant) {
        return feedbackRepository.findAllByRestaurant(restaurant);
    }

    @Override
    public Feedback getFeedbackById(Integer id) {
        return feedbackRepository.findFeedbackById(id);
    }

    @Override
    public Feedback addFeedback(Feedback feedback) {
        return feedbackRepository.save(feedback);
    }
}
