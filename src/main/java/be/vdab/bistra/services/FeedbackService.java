package be.vdab.bistra.services;

import be.vdab.bistra.models.FeedbackList;
import be.vdab.bistra.entities.*;

import java.util.List;

/**
 * @author Pelin Yuzbasioglu
 */

public interface FeedbackService {
    FeedbackList getFeedback(Restaurant restaurant);
    Integer countAllByRestaurant(Restaurant restaurant);
    List<Feedback> getAllByRestaurant(Restaurant restaurant);
    Feedback getFeedbackById(Integer id);
    Feedback addFeedback(Feedback feedback);
}
