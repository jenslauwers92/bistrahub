package be.vdab.bistra.services;

import be.vdab.bistra.entities.Menu;
import be.vdab.bistra.entities.Restaurant;


/**
 * @author Pelin Yuzbasioglu
 */
public interface MenuService {
    Menu findMenuById(int id);
    Menu updateMenu(Restaurant restaurant);
    Menu getMenuByRestaurantName();
}
