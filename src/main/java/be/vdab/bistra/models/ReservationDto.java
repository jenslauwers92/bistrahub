package be.vdab.bistra.models;

import java.time.LocalDate;


public class ReservationDto {
    private final int id;
    private final String name;
    private final int amountOfPeople;
    private final LocalDate date;
    private final Integer restaurant;
    private final Integer user;


    public ReservationDto(int id, String name, int amountOfPeople, LocalDate date, Integer restaurant, Integer user) {
        this.id = id;
        this.name = name;
        this.amountOfPeople = amountOfPeople;
        this.date = date;
        this.restaurant = restaurant;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAmountOfPeople() {
        return amountOfPeople;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getRestaurant() {
        return restaurant;
    }

    public int getUser() {
        return user;
    }
}
