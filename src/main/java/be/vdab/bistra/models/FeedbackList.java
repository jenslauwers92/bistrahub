package be.vdab.bistra.models;

import be.vdab.bistra.entities.Feedback;

import java.util.List;

public class FeedbackList {

    private final int amountOfReviews;
    private final double averageRating;
    private final List<Feedback> feedbacks;

    public FeedbackList(int amountOfReviews, double averageRating, List<Feedback> feedbacks) {
        this.amountOfReviews = amountOfReviews;
        this.averageRating = averageRating;
        this.feedbacks = feedbacks;
    }

    public int getAmountOfReviews() {
        return amountOfReviews;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }
}
