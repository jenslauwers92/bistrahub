package be.vdab.bistra.models;

import be.vdab.bistra.entities.AccountDetail;

import java.io.Serializable;
import java.util.List;

/**
 * @author Frederik Collignon
 */
public class AccountDetailsList implements Serializable {
    private List<AccountDetail> accountDetailList;

    public AccountDetailsList() {
    }

    public AccountDetailsList(List<AccountDetail> accountDetailList) {
        this.accountDetailList = accountDetailList;
    }

    public List<AccountDetail> getAccountDetailsList() {
        return accountDetailList;
    }

    public void setAccountDetailsList(List<AccountDetail> accountDetailList) {
        this.accountDetailList = accountDetailList;
    }
}
