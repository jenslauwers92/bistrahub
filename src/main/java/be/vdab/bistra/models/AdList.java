package be.vdab.bistra.models;

import be.vdab.bistra.entities.Advert;

import java.util.List;

public class AdList {

    List<Advert> adList;

    public AdList() {
    }

    public AdList(List<Advert> adList) {
        this.adList = adList;
    }

    public List<Advert> getAdList() {
        return adList;
    }

    public void setAdList(List<Advert> orderItemList) {
        adList = orderItemList;
    }

}
