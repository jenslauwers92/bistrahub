package be.vdab.bistra.models;

import be.vdab.bistra.entities.OrderItem;

import java.util.List;
/**
 * @author Bregt Pompen
 */
public class OrderItemList {

    List<OrderItem> OrderItemList;

    public OrderItemList() {
    }

    public OrderItemList(List<OrderItem> orderItemList) {
        OrderItemList = orderItemList;
    }

    public List<OrderItem> getOrderItemList() {
        return OrderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        OrderItemList = orderItemList;
    }
}
