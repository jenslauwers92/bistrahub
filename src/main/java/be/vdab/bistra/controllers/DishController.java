package be.vdab.bistra.controllers;

import be.vdab.bistra.domain.HttpResponse;
import be.vdab.bistra.domain.exception.JwtNoAccessException;
import be.vdab.bistra.domain.exception.MenuNotFoundException;
import be.vdab.bistra.entities.*;
import be.vdab.bistra.entities.enums.DishType;
import be.vdab.bistra.services.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static be.vdab.bistra.constant.MenuConstant.DISH_SUCCESSFULLY_DELETED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

/**
 * @author Pelin Yuzbasioglu
 */

@RestController
@Transactional
@RequestMapping(value="/menu/{menuId}/dish")
public class DishController {
    private final MenuService menuService;
    private final DishService dishService;
    private final RestaurantService restaurantService;

    public DishController(MenuService menuService, DishService dishService, RestaurantService restaurantService) {
        this.menuService = menuService;
        this.dishService = dishService;
        this.restaurantService = restaurantService;
    }

    @PostMapping
    public ResponseEntity<Dish> addDish(@PathVariable("menuId") Integer menuId,
                                        @RequestBody Dish dish, Principal principal) throws MenuNotFoundException, JwtNoAccessException {

        Menu menu = menuService.findMenuById(menuId);
        if (menu == null) {
            throw new MenuNotFoundException("Geen menu gevonden");
        }
        Restaurant restaurant = restaurantService.getRestaurantByMenuId(menu.getId());
        if (restaurant == null || !restaurant.getUser().getUsername().equals(principal.getName())) {
            throw new JwtNoAccessException();
        }
        dish.setMenu(menu);
        Dish newDish = dishService.addDish(dish);
        return new ResponseEntity<>(newDish, OK);
    }

    @PutMapping
    public ResponseEntity<Dish> updateDish(@RequestBody Dish dish, Principal principal) throws JwtNoAccessException {
        Restaurant restaurant = restaurantService.getRestaurantByMenuId(dish.getMenu().getId());
        if (restaurant == null || !restaurant.getUser().getUsername().equals(principal.getName())) {
            throw new JwtNoAccessException();
        }
        Dish updatedDish = dishService.updateDish(dish);
        return new ResponseEntity<>(updatedDish, OK);
    }

    @GetMapping
    public ResponseEntity<List<Dish>> getMenuDishes(@PathVariable("menuId") Integer menu_id) {
        List<Dish> dishes = dishService.getDishesByMenuId(menu_id);
        return new ResponseEntity<>(dishes, OK);
    }

    @GetMapping("/type")
    public ResponseEntity<List<Dish>> getDishesByType(@PathVariable("menuId") Integer menu_id) {
        List<Dish> dishes = dishService.getDishesByType(menu_id);
        return new ResponseEntity<>(dishes, OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Dish> getDish(@PathVariable("menuId") Integer menu_id,
                                        @PathVariable("id") Integer id) {
        Dish dish = dishService.getDishById(id);
        return new ResponseEntity<>(dish,OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpResponse> deleteDish(@PathVariable("menuId") Integer menu_id,
                                                   @PathVariable("id") Integer id, Principal principal) throws JwtNoAccessException {
        Dish dish = dishService.getDishById(id);
        Restaurant restaurant = restaurantService.getRestaurantByMenuId(dish.getMenu().getId());
        if (restaurant == null || !restaurant.getUser().getUsername().equals(principal.getName())) {
            throw new JwtNoAccessException();
        }
        dishService.deleteDish(id);
        return response(NO_CONTENT, DISH_SUCCESSFULLY_DELETED);
    }

    // === PRIVATE METHODS ===
    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
        HttpResponse body = new HttpResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase().toUpperCase(), message.toUpperCase());
        return new ResponseEntity<>(body, httpStatus);
    }
}
