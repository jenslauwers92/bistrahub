package be.vdab.bistra.controllers;

import be.vdab.bistra.entities.AccountDetail;
import be.vdab.bistra.models.AccountDetailsList;
import be.vdab.bistra.services.AccountDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

/**
 * @author Frederik Collignon
 */
@RestController
@RequestMapping("/accountDetails")
public class AccountDetailsController {
    private AccountDetailsService accountDetailsService;

    @Autowired
    public AccountDetailsController(AccountDetailsService accountDetailsService) {
        this.accountDetailsService = accountDetailsService;
    }

    @GetMapping()
    public ResponseEntity<AccountDetailsList> getAllAccountDetails() {
        return ResponseEntity.ok(new AccountDetailsList(accountDetailsService.findAllAccountDetails()));
    }

    @GetMapping(value = "/id/{id}")
    public ResponseEntity<AccountDetail> getAccountDetailsById(@PathVariable("id") int id) {
        AccountDetail accountDetail = accountDetailsService.findById(id);
        if(accountDetail != null) {
            return new ResponseEntity<>(accountDetail, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/email/{email}")
    public ResponseEntity<AccountDetail> getAccountDetailsByEmail(@PathVariable("email") String email) {
        AccountDetail accountDetail = accountDetailsService.findByEmail(email);
        if(accountDetail != null) {
            return new ResponseEntity<>(accountDetail, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/username/{username}")
    public ResponseEntity<AccountDetail> getAccountDetailsByUsername(@PathVariable("username") String username) {
        AccountDetail accountDetail = accountDetailsService.findByUserName(username);
        if(accountDetail != null) {
            return new ResponseEntity<>(accountDetail, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{username}/update")
    public ResponseEntity<AccountDetail> updateAccountDetailsById(@PathVariable("username") String username, @RequestParam Double addition ) {
//        Double addition = 5.0;
        AccountDetail newAccountDetail = accountDetailsService.updateCredits(username, addition);
        return new ResponseEntity<>(newAccountDetail, OK);
    }

//    @PutMapping()
//    public ResponseEntity<AccountDetails> updateAccountDetailsByEmail(@PathVariable String email, double addition) {
//        accountDetailsService.updateCredits(addition, email);
//        return ResponseEntity.ok().build();
//    }
}
