package be.vdab.bistra.controllers;

import be.vdab.bistra.domain.exception.ExceptionHandling;
import be.vdab.bistra.entities.Advert;
import be.vdab.bistra.repositories.AdvertRepository;
import be.vdab.bistra.services.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

import static org.springframework.http.HttpStatus.OK;

/**
 * @author Bregt Pompen
 */

@RestController
@RequestMapping("/adverts")
public class AdvertController extends ExceptionHandling {


    private final AdvertService adService;
    AdvertRepository adRepository;


    @Autowired
    public AdvertController(AdvertRepository adRepository, AdvertService adService) {
        this.adService = adService;
        this.adRepository = adRepository;
    }

    @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<Advert>> getAllHandler(){
        return ResponseEntity.ok(adService.getLast5Adverts());
    }

    @GetMapping(path = "/{id:^\\d+$}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Advert> getByIdHandler(@PathVariable("id") int id){
        Advert ad = adRepository.getOne(id);
        if(ad != null){
            return ResponseEntity.ok(ad);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PostMapping
    public ResponseEntity<Advert> create(@RequestBody Advert advert) {
        advert = adService.add(advert);
        System.out.println("create works!");
        return new ResponseEntity<>(advert, OK);
    }

}
