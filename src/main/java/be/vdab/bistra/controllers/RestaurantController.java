package be.vdab.bistra.controllers;

import be.vdab.bistra.domain.exception.ExceptionHandling;
import be.vdab.bistra.domain.exception.JwtNoAccessException;
import be.vdab.bistra.domain.exception.RestaurantNameExistsException;
import be.vdab.bistra.domain.exception.RestaurantNotFoundException;
import be.vdab.bistra.entities.*;
import be.vdab.bistra.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.nio.file.AccessDeniedException;
import java.security.Principal;
import java.util.List;

import static be.vdab.bistra.constant.RestaurantConstant.NO_RESTAURANT_FOUND;
import static org.springframework.http.HttpStatus.OK;

/**
 * @author Kenny Maes, Bregt Pompen, Pelin Yuzbasioglu
 */

@RestController
@RequestMapping(value="/restaurants")
public class RestaurantController extends ExceptionHandling {
    private final RestaurantService restaurantService;

    @Autowired
    public RestaurantController(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    // --- Endpoints ---

    @PutMapping("update/opening-hour")
    @PostAuthorize("hasAnyAuthority('owner', 'admin')")
    public ResponseEntity<OpeningHour> updateOpeningsHour(@RequestBody OpeningHour openingHour, Principal principal) throws Exception {
        OpeningHour newOpeningsHour = restaurantService.updateOpeningHour(openingHour);
        return ResponseEntity.ok(newOpeningsHour);
    }

    @GetMapping
    public ResponseEntity<List<Restaurant>> listOfRestaurants(){
        return ResponseEntity.ok(restaurantService.getRestaurants());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Restaurant> getRestaurant(@PathVariable("id") Integer id) throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantService.getRestaurant(id);
        return ResponseEntity.ok(restaurant);
    }

    @GetMapping("/{id}/opening-hours")
    public ResponseEntity<List<OpeningHour>> getOpeningHours(@PathVariable("id") Integer id) throws RestaurantNotFoundException {
        return ResponseEntity.ok(restaurantService.getOpeningHours(id));

    }
}
