package be.vdab.bistra.controllers;

import be.vdab.bistra.domain.exception.ExceptionHandling;
import be.vdab.bistra.domain.exception.RestaurantNotFoundException;
import be.vdab.bistra.entities.*;
import be.vdab.bistra.models.ReservationDto;
import be.vdab.bistra.services.*;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Pelin Yuzbasioglu
 */
@RestController
@Transactional
public class ReservationController extends ExceptionHandling {
    private final ReservationService reservationService;
    private final RestaurantService restaurantService;
    private final UserService userService;

    public ReservationController(ReservationService reservationService, RestaurantService restaurantService, UserService userService) {
        this.reservationService = reservationService;
        this.restaurantService = restaurantService;
        this.userService = userService;
    }

    @PostMapping("/users/{username}/reservations")
    public ResponseEntity<Reservation> create(@RequestBody ReservationDto reservationDto, @PathVariable("username") String username)
    throws RestaurantNotFoundException {
        User user = userService.findByUsername(username);
        Reservation reservation = new Reservation(reservationDto);
        reservation.setUser(user);
        Restaurant restaurant = restaurantService.getRestaurant(reservationDto.getRestaurant());
        reservation.setRestaurant(restaurant);
        Reservation newReservation = reservationService.addReservation(reservation);
        return ResponseEntity.ok(newReservation);
    }

    @GetMapping("/restaurants/{restaurantId}/reservations")
    public ResponseEntity<List<ReservationDto>> getReservations(@PathVariable("restaurantId") Integer restaurantId) throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
        return ResponseEntity.ok(reservationService.getAllByRestaurants(restaurant));
    }

    @GetMapping("/reservations/{id}")
    public ResponseEntity<Reservation> getReservation(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(reservationService.getReservationById(id));
    }
}
