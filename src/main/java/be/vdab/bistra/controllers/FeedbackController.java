package be.vdab.bistra.controllers;

import be.vdab.bistra.models.FeedbackList;
import be.vdab.bistra.domain.exception.*;
import be.vdab.bistra.entities.*;
import be.vdab.bistra.services.*;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Pelin Yuzbasioglu
 */
@RestController
@Transactional
@RequestMapping("/restaurants/{restaurantId}/feedbacks")
public class FeedbackController extends ExceptionHandling {

    private final FeedbackService feedbackService;

    private final RestaurantService restaurantService;

    public FeedbackController(FeedbackService feedbackService,
                              RestaurantService restaurantService) {
        this.feedbackService = feedbackService;
        this.restaurantService = restaurantService;
    }

    @PostMapping
    public ResponseEntity<Feedback> create(@RequestBody Feedback feedback, @PathVariable("restaurantId") Integer restaurantId) throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
        feedback.setRestaurant(restaurant);
        Feedback newFeedback = feedbackService.addFeedback(feedback);
        return ResponseEntity.ok(newFeedback);
    }

    @GetMapping
    public ResponseEntity<FeedbackList> getFeedbacks(@PathVariable("restaurantId") Integer restaurantId) throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
        return ResponseEntity.ok(feedbackService.getFeedback(restaurant));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Feedback> getFeedback(@PathVariable("restaurantId") Integer restaurantId, @PathVariable("id") Integer id) {
        return ResponseEntity.ok(feedbackService.getFeedbackById(id));
    }

    @GetMapping("/count")
    public ResponseEntity<Integer> getCount(@PathVariable("restaurantId") Integer restaurantId) throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
        return ResponseEntity.ok(feedbackService.countAllByRestaurant(restaurant));
    }

    @GetMapping("/average")
    public ResponseEntity<Double> getAverageRating(@PathVariable("restaurantId") Integer restaurantId) throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
        List<Feedback> allByRestaurant = feedbackService.getAllByRestaurant(restaurant);
        Double average = calculateAverage(allByRestaurant);
        return ResponseEntity.ok(average);
    }

    //=== Private Methods ===
    private Double calculateAverage(List<Feedback> allByRestaurant) {
        return allByRestaurant.stream().map(Feedback::getRating).collect(Collectors.averagingDouble(Double::valueOf));
    }


}
