package be.vdab.bistra.controllers;

import be.vdab.bistra.entities.Order;
import be.vdab.bistra.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/orders/")
public class OrderController {

    @Autowired
    OrderRepository orderRepository;

    @GetMapping(path = "/{id:^\\d+$}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Order> getByIdHandler(@PathVariable("id") int id){
        Order order = orderRepository.getOrderById(id);
        if(order != null){
            return ResponseEntity.ok(order);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}
