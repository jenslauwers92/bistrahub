package be.vdab.bistra.controllers;

import be.vdab.bistra.domain.exception.DrinkNotFoundException;
import be.vdab.bistra.domain.exception.JwtNoAccessException;
import be.vdab.bistra.entities.Dish;
import be.vdab.bistra.entities.Drink;
import be.vdab.bistra.entities.Menu;
import be.vdab.bistra.entities.Restaurant;
import be.vdab.bistra.services.DrinkService;
import be.vdab.bistra.services.MenuService;
import be.vdab.bistra.services.RestaurantService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

/**
 * @author Pelin Yuzbasioglu
 */

@RestController
@Transactional
@RequestMapping(value="/menu/{menuId}/drink")
public class DrinkController {
    private final DrinkService drinkService;
    private final MenuService menuService;
    private final RestaurantService restaurantService;

    public DrinkController(DrinkService drinkService, MenuService menuService, RestaurantService restaurantService) {
        this.drinkService = drinkService;
        this.menuService = menuService;
        this.restaurantService = restaurantService;
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('owner', 'admin')")
    public ResponseEntity<Drink> addDrink(@PathVariable("menuId") Integer menuId,
                                               @RequestBody Drink drink) {
        Menu menu = menuService.findMenuById(menuId);
        drink.setMenu(menu);
        Drink newDrink = drinkService.addDrink(drink);
        return new ResponseEntity<>(newDrink,OK);
    }

    @PutMapping
    @PreAuthorize("hasAnyAuthority('owner', 'admin')")
    public ResponseEntity<Drink> updateDrink(@PathVariable("menuId") Integer menuId,
                                             @RequestBody Drink drink, Principal principal) throws JwtNoAccessException {
        System.out.println(drink);
        Restaurant restaurant = restaurantService.getRestaurantByMenuId(drink.getMenu().getId());
        if (restaurant == null || !restaurant.getUser().getUsername().equals(principal.getName())) {
            throw new JwtNoAccessException();
        }
        Drink updatedDrink = drinkService.updateDrink(drink);
        return new ResponseEntity<>(updatedDrink, OK);
    }

    @GetMapping
    public ResponseEntity<List<Drink>> getMenuDrinks(@PathVariable("menuId") Integer menuId) {
        List<Drink> drinks = drinkService.getDrinksByMenuId(menuId);
        return new ResponseEntity<>(drinks,OK);
    }

    @GetMapping("/type")
    public ResponseEntity<List<Drink>> getDrinksByType(@PathVariable("menuId") Integer menu_id) {
        List<Drink> drinks = drinkService.getDrinksByType(menu_id);
        return new ResponseEntity<>(drinks, OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Drink> getDrink(@PathVariable("menuId") Integer menu_id,
                                          @PathVariable("id") Integer id) throws DrinkNotFoundException {
        Drink drink = drinkService.getDrinkById(id);
        return new ResponseEntity<>(drink,OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('owner', 'admin')")
    public ResponseEntity<Drink> deleteDrink(@PathVariable("menuId") Integer menu_id,
                                             @PathVariable("id") Integer id, Principal principal) throws JwtNoAccessException, DrinkNotFoundException {
        Drink drink = drinkService.getDrinkById(id);
        Restaurant restaurant = restaurantService.getRestaurantByMenuId(drink.getMenu().getId());
        if (restaurant == null || !restaurant.getUser().getUsername().equals(principal.getName())) {
            throw new JwtNoAccessException();
        }
        drinkService.deleteDrink(id);
        return ResponseEntity.noContent().build();
    }
}
