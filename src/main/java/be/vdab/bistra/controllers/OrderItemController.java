package be.vdab.bistra.controllers;

import be.vdab.bistra.entities.OrderItem;
import be.vdab.bistra.models.OrderItemList;
import be.vdab.bistra.repositories.OrderItemRepository;
import be.vdab.bistra.services.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Bregt Pompen
 */

@RestController
@RequestMapping("/orderitem") // ??
public class OrderItemController {

    @Autowired
    OrderItemRepository orderItemRepository;

    @Autowired
    OrderItemService orderItemService;



    @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OrderItemList> getAllHandler(){
        return ResponseEntity.ok(new OrderItemList(orderItemRepository.findAll()));
    }


    @GetMapping(path = "/{id:^\\d+$}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<OrderItem> getByIdHandler( @PathVariable("id") int id){
        OrderItem orderItem = orderItemRepository.getOrderItemById(id);
        if(orderItem != null){
            return ResponseEntity.ok(orderItem);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @DeleteMapping(path = "/{id:^\\d+$}")
    public ResponseEntity<?> deleteHandler(@PathVariable int id){
        orderItemRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

//    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
//            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
//    public ResponseEntity<OrderItem> postHandler(@RequestBody OrderItem orderItem){
//        if( orderItem.getId() <= 0){
//            orderItemRepository.save(orderItem);
//            return ResponseEntity.ok(orderItem);
//        }
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
//    }

    @PostMapping
    public ResponseEntity<List<OrderItem>> SaveAllHandler(@RequestBody  List<OrderItem> orderItems){
        for(OrderItem model : orderItems) {
            System.out.println("total price of orderItem = " + model.getPrice());
        }
        String userDetails = (String)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println("this user: " +  userDetails);
        orderItemService.ProcessAll(orderItems, userDetails );
        return ResponseEntity.ok(orderItems);
    }

}
