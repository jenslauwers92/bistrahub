package be.vdab.bistra.controllers;

import be.vdab.bistra.domain.exception.*;
import be.vdab.bistra.entities.*;
import be.vdab.bistra.services.RestaurantService;
import be.vdab.bistra.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

/**
 * @author Kenny Maes, Pelin Yuzbasioglu
 */

@RestController
@RequestMapping(value="/owner/restaurants")
public class RestaurantControllerOwner extends ExceptionHandling {
    private final RestaurantService restaurantService;
    private final UserService userService;

    @Autowired
    public RestaurantControllerOwner(RestaurantService restaurantService, UserService userService) {
        this.restaurantService = restaurantService;
        this.userService = userService;
    }

    // --- Endpoints ---


    @PostMapping
    @PreAuthorize("hasAnyAuthority('owner', 'admin')")
    public ResponseEntity<Restaurant> addRestaurant(@RequestBody Restaurant restaurant, Principal principal) throws RestaurantNotFoundException, RestaurantNameExistsException, UserNotFoundException {
        User user = userService.findByUsername(principal.getName());

        if (user == null) throw new UserNotFoundException("Je kan geen restaurant aanmaken");

        Restaurant newRestaurant = restaurantService.addNewRestaurant(
                restaurant,
                restaurant.getName(),
                restaurant.getType(),
                restaurant.getRating(),
                restaurant.getMaxCapacity(),
                restaurant.isParking(),
                restaurant.getDescription(),
                new Menu(),
                user);
        return new ResponseEntity<>(newRestaurant, OK);
    }


    @PutMapping
    @PreAuthorize("hasAnyAuthority('owner', 'admin')")
    public ResponseEntity<Restaurant> updateRestaurant(@RequestBody Restaurant restaurant, Principal principal) throws RestaurantNotFoundException, JwtNoAccessException, AddressNotFoundException {
        if (restaurant.getId() == 0) throw new RestaurantNotFoundException("Het restaurant dat je wil updaten bestaat niet");
        Restaurant currentRestaurant = restaurantService.getRestaurant(restaurant.getId());

        // throw exception when logged in user is not the same as restaurant owner
        if (!currentRestaurant.getUser().getUsername().equals(principal.getName())) throw new JwtNoAccessException();
        restaurant.setUser(currentRestaurant.getUser());
        restaurant.setMenu(currentRestaurant.getMenu());

        // update the restaurant
        Restaurant updatedRestaurant = restaurantService.updateRestaurant(restaurant);
        return new ResponseEntity<>(updatedRestaurant, OK);
    }


    @GetMapping
    @PreAuthorize("hasAnyAuthority('owner', 'admin')")
        public ResponseEntity<List<Restaurant>> getOwnerRestaurants(Principal principal) {
        List<Restaurant> restaurants = restaurantService.getRestaurantByUsername(principal.getName());
        return new ResponseEntity<>(restaurants, OK);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('owner', 'admin')")
    public ResponseEntity<Restaurant> getOwnerRestaurant(@PathVariable("id") Integer id, Principal principal) throws RestaurantNotFoundException, JwtNoAccessException {
        Restaurant restaurant = restaurantService.getRestaurant(id);
        if (!restaurant.getUser().getUsername().equals(principal.getName())){
            throw new JwtNoAccessException();
        }
        return ResponseEntity.ok(restaurant);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('owner', 'admin')")
    public ResponseEntity<Drink> deleteRestaurant(@PathVariable("id") Integer id, Principal principal, Authentication authentication) throws RestaurantNotFoundException, JwtNoAccessException {
        Restaurant restaurant = restaurantService.getRestaurant(id);

        // Check if logged in user is same as user from restaurant OR logged in user has admin role
        if (!restaurant.getUser().getUsername().equals(principal.getName()) | !authentication.getAuthorities().contains("admin")) {
            throw new JwtNoAccessException();
        }
        restaurantService.deleteRestaurant(id);
        return ResponseEntity.noContent().build();
    }
    
}
