package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Feedback;
import be.vdab.bistra.entities.Restaurant;
import be.vdab.bistra.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

/**
 * @author Pelin Yuzbasioglu
 */


public interface FeedbackRepository extends JpaRepository<Feedback, Integer> {

    List<Feedback> findAllByRestaurant(Restaurant restaurant);
    Integer countAllByRestaurant(Restaurant restaurant);
    Feedback findFeedbackById(Integer id);
}
