package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.OpeningHour;
import be.vdab.bistra.entities.Restaurant;
import be.vdab.bistra.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Frederik Collignon, Pelin Yuzbasioglu
 */
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {

      public Restaurant getRestaurantById(int id);
//    public List<Restaurant> getRestaurantByName(String name);
//    public List<Restaurant> getRestaurantByType(KitchenType type);
//    public List<Restaurant> getRestaurantByRating(Rating rating);
//    public List<Restaurant> getRestaurantByAddress(Address address);
      public List<Restaurant> getRestaurantsByUser(User user);
      public Restaurant getRestaurantByName(String name);
      Restaurant getRestaurantByMenu_Id(Integer menuId);

}
