package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Menu;
import be.vdab.bistra.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author Frederik Collignon
 */

public interface MenuRepository extends JpaRepository<Menu, Integer> {
    public Menu getMenuById(int id);
}
