package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.AccountDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Frederik Collignon
 */

public interface AccountDetailsRepository extends JpaRepository<AccountDetail, Integer> {
    public AccountDetail findAccountDetailById(int id);
    public AccountDetail findAccountDetailByEmail(String email);
}
