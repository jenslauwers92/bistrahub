package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author Frederik Collignon
 */
public interface OrderItemRepository extends JpaRepository<OrderItem, Integer> {
    OrderItem getOrderItemById(int id);
    //public List<OrderItem> findOrderItemByOrders(List<Order> orders);
}
