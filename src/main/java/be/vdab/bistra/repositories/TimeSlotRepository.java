package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.TimeSlot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalTime;
import java.util.List;

/**
 * @Author Frederik Collignon
 */
public interface TimeSlotRepository extends JpaRepository<TimeSlot, Integer> {
    public List<TimeSlot> findTimeSlotByStartHour(LocalTime startHour);
    public List<TimeSlot> findTimeSlotByEndHour(LocalTime endHour);
}
