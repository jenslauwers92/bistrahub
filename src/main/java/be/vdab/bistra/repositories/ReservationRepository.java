package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Reservation;
import be.vdab.bistra.entities.Restaurant;
import be.vdab.bistra.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Frederik Collignon, Pelin Yuzbasioglu
 */
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
    public Reservation findReservationById(Integer id);
    public List<Reservation> findReservationByDate(LocalDate date);
    public List<Reservation> findReservationsByRestaurant(Restaurant restaurant);
    public List<Reservation> findReservationsByUser(User user);
}
