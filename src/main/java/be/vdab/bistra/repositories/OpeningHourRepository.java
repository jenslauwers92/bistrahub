package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.OpeningHour;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OpeningHourRepository extends JpaRepository<OpeningHour, Integer> {

    OpeningHour findOpeningHourById(Integer id);
}
