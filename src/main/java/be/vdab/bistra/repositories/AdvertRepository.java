package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Advert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Bregt Pompen
 */

public interface AdvertRepository extends JpaRepository<Advert, Integer> {

    //public List<Advert> findAdvertsById(int id);


}
