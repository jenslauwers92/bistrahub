package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Order;
import be.vdab.bistra.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author Frederik Collignon
 */

public interface OrderRepository extends JpaRepository<Order, Integer> {
    public Order getOrderById(int id);
//    public List<Order> findOrderByAccountDetails(AccountDetails accountDetails);
    Order findOrderByUser(User user);

}
