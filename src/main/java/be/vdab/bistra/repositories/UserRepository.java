package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.AccountDetail;
import be.vdab.bistra.entities.User;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Bregt Pompen
 */
public interface UserRepository extends JpaRepository<User,Integer> {

    User findUserByUsername(String username);
    User findUserByAccountDetail_Email(String email);
}
