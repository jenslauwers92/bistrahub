package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Dish;
import be.vdab.bistra.entities.enums.DishType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Frederik Collignon, Pelin Yuzbasioglu
 */
public interface DishRepository extends JpaRepository<Dish, Integer> {
//    public List<Dish> findDishByNameOrderByPrice(String name);
//    public List<Dish> findDishByTypeOrderByPrice(DishType type);
//    public List<Dish> findDishByVegetarianOrderByPrice(boolean vegetarian);
//    public List<Dish> findDishByVeganOrderByPrice(boolean vegan);
//    public List<Dish> findDishByContainsNutsOrderByPrice(boolean containsNuts);
//    public List<Dish> findDishByMenu(Menu menu);
//    public List<Dish> findDishByOrderItem(OrderItem orderItem);
    public Dish findDishById(int id);
    public List<Dish> findDishesByMenu_Id(int menu_id);
    public List<Dish> findDishesByMenu_IdOrderByType(int menu_id);
}
