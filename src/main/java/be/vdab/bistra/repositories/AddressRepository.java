package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Address;
import be.vdab.bistra.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author Frederik Collignon
 */

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {
    public List<Address> findAddressByStreetIgnoreCase(String street);
    public List<Address> findAddressByStreetLike(String street);
    public List<Address> findAddressByCountryIgnoreCase(String country);
}
