package be.vdab.bistra.repositories;

import be.vdab.bistra.entities.Drink;
import be.vdab.bistra.entities.enums.DrinkType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Frederik Collignon, Pelin Yuzbasioglu
 */

public interface DrinkRepository extends JpaRepository<Drink, Integer> {
      public List<Drink> findDrinksByMenu_Id(int menu_id);
      public List<Drink> findDrinksByMenu_IdOrderByType(int menu_id);
}
