package be.vdab.bistra.constant;

public class RestaurantConstant {
    public static final String NO_RESTAURANT_FOUND = "Geen restaurant gevonden in databank!";
    public static final String RESTAURANT_NAME_ALREADY_EXISTS = "De restaurant naam werd al aangenomen!";
}
