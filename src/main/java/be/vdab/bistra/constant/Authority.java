package be.vdab.bistra.constant;

public class Authority {
    public static final String[] USER_AUTHORITIES = { "user" };
    public static final String[] OWNER_AUTHORITIES = { "user", "owner" };
    public static final String[] SUPER_ADMIN_AUTHORITIES = { "user", "owner", "admin" };
}
