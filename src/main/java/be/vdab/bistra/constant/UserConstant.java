package be.vdab.bistra.constant;

public class UserConstant {
    public static final String NO_USER_FOUND_BY_EMAIL = "No user found for email: ";
    public static final String EMAIL_ALREADY_EXISTS = "Email bestaat al";
    public static final String USERNAME_ALREADY_EXISTS = "Gebruikersnaam bestaat al";
    public static final String NO_USER_FOUND_BY_USERNAME = "No user found by username: ";
    public static final String EMAIL_SENT = "An email with a new password was sent to: ";
    public static final String USER_DELETED_SUCCESSFULLY = "User deleted successfully";

}
